
def start():
    from tornado_app import application
    try:
        application.run()
    finally:
        application.shutdown()
