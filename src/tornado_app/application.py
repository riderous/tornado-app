
from pymongo import MongoClient
from tornado import web, ioloop
from tornado.options import options

from tornado_app.routes import get_routes
from tornado_app.settings import settings


class Application(web.Application):

    @classmethod
    def app_settings(cls):
        for key, value in settings.iteritems():
            if key in options:
                options[key] = value
        return settings

    def db_connect(self):
        uri = 'mongodb://{}:{}@{}:{}/{}'
        uri_format = uri.format(settings['db_user'], settings['db_pass'], settings['db_host'],
                                settings['db_port'], settings['db_name'])
        conn = MongoClient(uri_format)

        self.db = conn[settings['db_name']]


def run():
    app_settings = Application.app_settings()
    app = Application(get_routes(), **app_settings)
    app.db_connect()
    app.listen(app_settings['on_port'])
    ioloop.IOLoop.instance().start()


def shutdown():
    pass
