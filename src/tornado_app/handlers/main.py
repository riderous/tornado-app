
from tornado import web


class MainHandler(web.RequestHandler):
    def get(self):
        if not self.application.db.counter.find_one({'name': 'main_handler'}):
            self.application.db.counter.insert({'name': 'main_handler', 'n': 0})
        self.application.db.counter.update({'name': 'main_handler'}, {'$inc': {'n': 1}})

        main_handler_counter = self.application.db.counter.find_one({'name': 'main_handler'}, {'n': 1})
        self.write('Calls: {}'.format(main_handler_counter['n']))
