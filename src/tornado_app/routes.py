
from tornado_app.handlers.main import MainHandler


routes = [
    (r'/', MainHandler)
]


def get_routes():
    return routes
