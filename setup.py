
from setuptools import setup, find_packages

install_requires = [
    'tornado==3.1.1',
    'motor==0.1.2',
]

setup(name='tornado_app', install_requires=install_requires,
      packages=find_packages('src'), package_dir={'': 'src'},
      entry_points={'console_scripts': ['app-start = tornado_app.entrypoints:start']})
